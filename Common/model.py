"""
数据库映射对象
"""
from sqlalchemy import create_engine,Column,Integer,String,Float,func,and_,or_,Text,\
    ForeignKey,Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker,relationship,backref


Base = declarative_base()

# 关系表  多对多
financial_category_class = Table(
    'financial_category_class',#第三张表名
    Base.metadata, #元类的数据
    Column('category',Integer,ForeignKey('financial_category.id'),primary_key=True),
    Column('class',Integer,ForeignKey('financial_class.id'),primary_key=True),
    #两字段primary_key都等于True，组合主键唯一，防止内容一样
)

financial_category_data_class = Table(
    'financial_category_data_class',
    Base.metadata,
    Column('financial_category',Integer,ForeignKey('financial_category.id'),primary_key=True),
    Column('data_class',Integer,ForeignKey('data_class.id'),primary_key=True),
)


# 真实对象表
class FinancialCategory(Base):
    __tablename__ = 'financial_category'
    id = Column(Integer, primary_key=True, autoincrement=True)
    category = Column(String(50), nullable=False)
    describe = Column(String(100), nullable=True)
    # 反向关联，financial_class关联的表，financial_category同financial_class_tags关联financial_class，financial_class通过financial_categories反向关联，
    # secondary就是第三张表
    financial_classes = relationship('FinancialClass', backref='financial_categories', lazy='subquery', secondary=financial_category_class)
    data_classes = relationship('DataClass', backref='financial_categories', lazy='subquery', secondary=financial_category_data_class)

class FinancialClass(Base):
    __tablename__ = 'financial_class'
    id = Column(Integer, primary_key=True, autoincrement=True)
    cls = Column(String(50), nullable=False)
    describe = Column(String(100), nullable=True)
    classmembers = relationship('FinancialClassMember',lazy='subquery',backref='financial_class')

class FinancialClassMember(Base):
    __tablename__ = 'financial_classmember'
    id = Column(Integer, primary_key=True, autoincrement=True)
    classmember = Column(String(50), nullable=False)
    describe = Column(String(100), nullable=True)
    financial_class_id = Column(Integer,ForeignKey('financial_class.id'))

class DataClass(Base):
    __tablename__ = 'data_class'
    id = Column(Integer, primary_key=True, autoincrement=True)
    cls = Column(String(50), nullable=False)
    describe = Column(String(100), nullable=True)
    classmembers = relationship('DataClassMember',lazy='subquery',backref='data_class')

class DataClassMember(Base):
    __tablename__ = 'data_classmember'
    id = Column(Integer, primary_key=True, autoincrement=True)
    classmember = Column(String(50), nullable=False)
    describe = Column(String(100), nullable=True)
    data_class_id = Column(Integer, ForeignKey('data_class.id'))




