from enum import Enum

class DataPeriod(Enum):
    Tick = "tick"
    M1 = "m1"
    M5 = "m5"
    M15 = "m15"
    M30 = "m30"
    M60 = "m60"
    Day = "day"
    Month = "month"
    Year = "year"
