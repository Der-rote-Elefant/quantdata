# 量化数据库结构
* Mysql数据库 quant_data_info：记录所有入库数据之间的关系
* Mysql数据库 quant_trade_info：记录每日与交易相关的信息
* Clickhouse :存储入库数据


# DataDaily: 每日数据更新脚本
* 股票个股行情(Day,M1)
* 股票指数行情(Day,M1)
* 股票指数成分


# DataSaveHistory： 历史数据入库脚本
* 股票行情


# DataManager: 数据管理模块
## UI方式数据查询
自个儿乱按凑活用 没反应就是出bug了


## No UI方式数据查询
Mysql只读账户:
shentu_reader  shentu123



金融大类<br>
FinancialCategory
```
大类名称： category:str
大类描述: describe:str
金融分类列表: financial_classes:List[FinancialClass]
数据大类列表： data_classes:List[FinancialClass]
```


金融大类分类<br>
FinancialClass
```
分类名称: cls:str
分类描述: describe:str
分类成员组成： classmembers:List[FinancialClassMember]
```

金融大类分类具体组成<br>
FinancialClassMember
```
分类成员名称: classmember:str
分类成员描述: describe:str
```


数据分类<br>
DataClass
```
数据分类名称: cls:str
分类描述: describe:str
数据分类参数成员: classmembers:List[DataClassMember]
```


数据分类具体参数成员<br>
DataClassMember
```
具体参数名称：classmember:str
参数描述: describe:str
```

注:
要找金融大类A的所有数据（除了index类别之外,例如stock，option）,
选择FinancialClass=A_index,FinancialClassMember选择all_A

举例:<br>
不写了 太烦了

