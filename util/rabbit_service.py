import pika


class RabbitService(object):
    def __init__(self, host, port, username, password):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.exchange_type = "fanout"
        self.pub_connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=host, port=port,
                                      credentials=pika.PlainCredentials(username=username, password=password)))

    def subscribe(self, topic, func):
        sub_connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self.host, port=self.port,
                                      credentials=pika.PlainCredentials(username=self.username,
                                                                        password=self.password)))
        channel = sub_connection.channel()
        channel.exchange_declare(exchange=topic, exchange_type=self.exchange_type)
        queue = channel.queue_declare(queue="", exclusive=True, auto_delete=True)
        queue_name = queue.method.queue
        print(queue_name)
        channel.queue_bind(exchange=topic, queue=queue_name)

        def call_back(ch, method, properties, body):
            message = str(body, encoding="utf-8")
            func(message)

        channel.basic_consume(queue_name, call_back, auto_ack=True, )
        channel.start_consuming()

    def publish(self, topic, message):
        channel = self.pub_connection.channel()
        channel.exchange_declare(exchange=topic,
                                 exchange_type=self.exchange_type)
        channel.basic_publish(
            exchange=topic,
            routing_key='',
            body=message
        )


if __name__ == '__main__':
    rabbit_service = RabbitService(host="192.168.1.144", port=5672, username="root", password="Baofang660")
    rabbit_service.subscribe("sub.tick", func=lambda x: print(x))
