from clickhouse_driver import Client
import pandas as pd
import re

client = Client(host='xxx', database='xxx', user='xxx', password='xxx')


def read_sql(sql):
    data, columns = client.execute(sql, columnar=True, with_column_types=True)
    df = pd.DataFrame({re.sub(r'\W', '_', col[0]): d for d, col in zip(data, columns)})
    return df


def get_type_dict(tb_name):
    sql = f"select name, type from system.columns where table='{tb_name}';"
    df = read_sql(sql)
    df = df.set_index('name')
    type_dict = df.to_dict('dict')['type']

    return type_dict


def to_sql(df, tb_name):
    type_dict = get_type_dict(tb_name)
    columns = list(type_dict.keys())
    # 类型处理
    for i in range(len(columns)):
        col_name = columns[i]
        col_type = type_dict[col_name]
        if 'Date' in col_type:
            df[col_name] = pd.to_datetime(df[col_name])
        elif 'Int' in col_type:
            df[col_name] = df[col_name].astype('int')
        elif 'Float' in col_type:
            df[col_name] = df[col_name].astype('float')
        elif col_type == 'String':
            df[col_name] = df[col_name].astype('str').fillna('')
    # df数据存入clickhouse
    cols = ','.join(columns)
    data = df.to_dict('records')
    client.execute(f"INSERT INTO {tb_name} ({cols}) VALUES", data, types_check=True)
