import urllib
import urllib.parse
import urllib.request
import requests
import config
class Wechat(object):
	"""
	发送消息给微信公众号
	"""

	wechat_url = "http://www.wdl421.cn/api/WeChatMSGapi.php?user={0}&data={1}"

	def __init__(self, users=[]):
		self.users = users

	def send_message(self, message):
		for user in self.users:
			try:
				request_url = Wechat.wechat_url.replace("{0}", user)
				request_url = request_url.replace("{1}", urllib.parse.quote(message))
				print(request_url)
				urllib.request.urlopen(request_url)
			except Exception as e:
				print("用户：%s 发送微信公众号消息出错：%s" % (user, e))


class DingTalk(object):

	url = config.dingtalk_url

	def send_message_to_dingtalk(self,msg):
		try:
			r = requests.get(DingTalk.url, params={'msg': msg, 'send_all': False})
		except Exception as e:
			print(e)

if __name__ == '__main__':
	wechat = Wechat(users=["DDC"])
	wechat.send_message("test")
	print(wechat)
