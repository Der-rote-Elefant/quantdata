# -*- coding: utf-8 -*-
import pandas as pd
import pymongo


class MongoDatabase(object):

    def __init__(self, user_id, password, host, port, db_name):
        url = "mongodb://{0}:{1}@{2}".format(user_id, password, host)
        self.conn = pymongo.MongoClient(host=url, port=port)
        self.db = self.conn[db_name]

    def get_state(self):
        return self.conn is not None and self.db is not None

    def insert_one(self, collection, data):
        if self.get_state():
            ret = self.db[collection].insert_one(data)
            return ret.inserted_id
        else:
            return ""

    def insert_dataframe(self, collection, df):
        if self.get_state():
            data = self.__dataframe_to_json(df)
            print(data)
            ret = self.db[collection].insert_many(data, False)
            return ret
        else:
            return ""

    def update_dataframe(self, collection, df):
        if self.get_state():
            data = self.__dataframe_to_json(df)
            ret = self.update_dict(collection, data, False)
            return ret
        else:
            return ""

    def insert_many(self, collection, data):
        if self.get_state():
            ret = self.db[collection].insert_many(data, False)
            return ret.inserted_ids
        else:
            return ""

    def update(self, collection, upkeydate, data, upsert=False):
        # data format:
        # {key:[old_data,new_data]}
        data_filter = {}
        data_revised = {}
        for key in upkeydate.keys():
            data_filter[key] = upkeydate[key][1]
        for key in data.keys():
            data_revised[key] = data[key]
        if self.get_state():
            return self.db[collection].update_many(data_filter, {"$set": data_revised}, upsert).modified_count

        return 0

    def find(self, col, condition, column=None):
        if self.get_state():
            if column is None:
                return self.db[col].find(condition)
            else:
                return self.db[col].find(condition, column)
        else:
            return None

    def delete(self, col, condition):
        if self.get_state():
            return self.db[col].delete_many(filter=condition).deleted_count
        return 0

    def __dataframe_to_json(self, df):
        result = list()
        columns = df.columns.values.tolist()
        for index, row in df.iterrows():
            row_dict = dict()
            for column in columns:
                row_dict[column] = row[column]
            result.append(row_dict)
        return result

    def update_dict(self, collection, data=None, update=True):
        if not data:
            return
        if self.get_state():
            bluk = self.db[collection].initialize_ordered_bulk_op()
            update_key = "$set" if update else "$setOnInsert"
            for i in data:
                _id = i.pop('_id')
                bluk.find({'_id': _id}).upsert().update({update_key: i})
            result = bluk.execute()
            return result
        else:
            return 0


class MongoDBHelpIPPortList(object):
    def __init__(self, ip_port_list, user, pwd, set_name, database):
        self.ip_port_list = ip_port_list
        self.user = user
        self.pwd = pwd
        self.set_name = set_name
        self.conn_url = self.get_mongo_conn_url_replicaset()
        self.conn = pymongo.MongoClient(host=self.conn_url)
        self.db = self.conn[database]

    def get_mongo_conn_url_replicaset(self):
        url = 'mongodb://'
        if self.user is not None:
            if self.pwd is None:
                self.pwd = self.user
            url += '%s:%s@' % (self.user, self.pwd)
        url += ','.join(self.ip_port_list)
        if self.set_name is not None:
            url += '/?replicaSet=%s' % self.set_name
        return url

    def get_state(self):
            return self.conn is not None and self.db is not None

    def insert_one(self, collection, data):
        if self.get_state():
            ret = self.db[collection].insert_one(data)
            return ret.inserted_id
        else:
            return ""

    def insert_many(self, collection, data):
        if self.get_state():
            ret = self.db[collection].insert_many(data.list,False)
            return ret.inserted_ids
        else:
            return ""

    def update(self, collection, upkeydate, data, upsert=False):
        # data format:
        # {key:[old_data,new_data]}
        data_filter = {}
        data_revised = {}
        for key in upkeydate.keys():
            data_filter[key] = upkeydate[key][1]
        for key in data.keys():
            data_revised[key] = data[key]
        if self.get_state():
            return self.db[collection].update_many(data_filter, {"$set": data_revised},upsert).modified_count
        return 0

    def update_many(self, collection, value = None, update = True):
        if not value:
            return
        if self.get_state():
            # self.db[collection].update_one({'_id':docs['_id']},{"$set":value},upsert=True)
            # return 0;
            bluk = self.db[collection].initialize_ordered_bulk_op()
            update_key = "$set" if update else "$setOnInsert"
            for i in value.mongodb_bar_list:
                bluk.find({'_id':i.key['_id']}).upsert().update({update_key:i.value})
            result = bluk.execute()
            return result
            # result = self.db[collection].bulk_write(ns)
            # return result
        else:
            return 0

    def find(self, col, condition, column=None):
        if self.get_state():
            if column is None:
                return self.db[col].find(condition)
            else:
                return self.db[col].find(condition, column)
        else:
            return None

    def delete(self, col, condition):
        if self.get_state():
            return self.db[col].delete_many(filter=condition).deleted_count
        return 0


if __name__ == '__main__':
    db = MongoDatabase('root', 'Baofang660', '192.168.1.184', 27017, 'Close')
    print(db.get_state())
