#!/usr/bin/env python
# -*- coding:utf-8 -*-

import pandas as pd
import os
import datetime
import copy
from util import mongo_db
from queue import Queue
import threading
import pytz


class Bar:
    def __init__(self, row, code):
        # row = row[:-1].split(',')
        self.code = code
        self.date = row['date']
        self.open = float(row['open'])
        self.high = float(row['high'])
        self.low = float(row['low'])
        self.close = float(row['close'])
        self.volume = float(row['volume'])
        self.op = float(row['openInterest'])
        self.vwap = 0
        if self.volume != 0:
            self.vwap = self.op / self.volume

    def add_bar(self, bar1, ax=1):
        if ax == 2:
            self.open = bar1.open
        else:
            self.close = bar1.close
        if self.high < bar1.high:
            self.high = bar1.high
        if self.low > bar1.low:
            self.low = bar1.low
        self.volume = self.volume + bar1.volume
        self.op = self.op + bar1.op
        if self.volume != 0:
            self.vwap = self.op / self.volume


class CycleBar(object):
    def __init__(self, bar_list, cycle):
        self.bar_list = bar_list
        self.cycle = cycle
        self.DBName = format('M' + cycle)
        self.mongodb_bar_list = []
        for i in bar_list:
            self.mongodb_bar_list.append(MongodbBar(i))


class MongodbBar(object):
    def __init__(self, bar):
        self.pacific = pytz.timezone('Asia/Shanghai')
        self.key = {
            '_id': format(bar.code + '_' + bar.date.strftime("%Y-%m-%d %H:%M:%S"))
        }
        self.value = {
            'Close': bar.close,
            'Code': bar.code,
            'Date': self.pacific.localize(bar.date),
            'High': bar.high,
            'Low': bar.low,
            'Open': bar.open,
            'OpenInterest': bar.op,
            'Volume': bar.volume / 100,
            'VWAP': round(bar.vwap, 2)
        }


class MongodbBarList(object):
    def __init__(self, bar_list):
        self.list = []
        for bar in bar_list:
            value = {
                '_id': format(bar.code + '_' + bar.date.strftime("%Y-%m-%d %H:%M:%S")),
                'Close': bar.close,
                'Code': bar.code,
                'Date': bar.date,
                'High': bar.high,
                'Low': bar.low,
                'Open': bar.open,
                'OpenInterest': bar.op,
                'Volume': bar.volume / 100,
                'VWAP': round(bar.vwap, 2)
            }
            self.list.append(value)


class Consumer(threading.Thread):
    def __init__(self, t_name, queue, t_max, mongodb):
        threading.Thread.__init__(self, name=t_name)
        self.name = t_name
        self.q = queue
        self.max = t_max
        self.mongodb = mongodb

    def run(self):
        while True:
            for i in range(self.max):
                val = self.q.get()
                print(self.name, i, len(val.bar_list), val.cycle)
                self.mongodb.update_many(val.DBName, val, True)
                self.q.task_done()


class THSMinBar:
    def __init__(self, path_list):
        start_time = datetime.datetime.now()
        ip_port_list = ['192.168.1.173:27017']
        self.db = mongo_db.MongoDBHelpIPPortList(ip_port_list=ip_port_list, user='root', pwd='Baofang660',
                                                 set_name=None, database='Stock')
        self.q = Queue(15)
        w = Consumer('T', self.q, 5, mongodb=self.db)
        w.setDaemon(True)
        w.start()
        for path in path_list:
            self.bar_list(path, True)
        end_time = datetime.datetime.now()
        print("转换完成。")
        print(start_time, end_time)
        print((end_time - start_time).seconds)
        self.q.join()

    def bar_list(self, path, add=True):
        print(path)
        df = pd.DataFrame()
        if add is False:
            df = pd.read_csv(path, names=['date', 'open', 'high', 'low', 'close', 'volume', 'openInterest'])
            code = os.path.split(path)[-1].split('.csv')[0]
            df['code'] = code
            df['day'] = df.apply(lambda x: x.date[0:10], axis=1)
        else:
            df = pd.read_csv(path, names=['code', 'date', 'open', 'high', 'low', 'close', 'volume', 'openInterest'])
            df['day'] = df['date'][0][0:10]
        df = df.fillna(0)
        code_list = df.drop_duplicates(['code'])
        for Code in code_list['code']:
            print(Code)
            code_df = df.loc[df['code'] == Code]
            date_list = []
            for i in code_df['day']:
                if i not in date_list:
                    date_list.append(i)
            for i in date_list:
                print(i)
                day_df = code_df.loc[df["day"] == i]
                day_df["date"] = pd.to_datetime(day_df["date"], format="%Y-%m-%d %H:%M")
                # daydf['date'] = daydf['date'].apply(lambda x: pacific.localize(x))
                day_df.sort_index(axis=0, ascending=True, by='date')
                one_min_bar_list = self.one_min_bar(Code, day_df)
                if one_min_bar_list is not None:
                    self.q.put(CycleBar(one_min_bar_list, '1'))
                    five_min_bars = self.compose_bar(one_min_bar_list, 300)
                    self.q.put(CycleBar(five_min_bars, '5'))
                    fifteen_bars = self.compose_bar(five_min_bars, 900)
                    self.q.put(CycleBar(fifteen_bars, '15'))
                    thirty_bars = self.compose_bar(fifteen_bars, 1800)
                    self.q.put(CycleBar(thirty_bars, '30'))
                    one_hour_bars = self.compose_bar(fifteen_bars, 3200)
                    self.q.put(CycleBar(one_hour_bars, '60'))
                    two_hour_bars = self.compose_bar(fifteen_bars, 6400)
                    self.q.put(CycleBar(two_hour_bars, '120'))

    def list_all_files(self, dic_path):
        import os
        _files = []
        dir_list = os.listdir(dic_path)  # 列出文件夹下所有的目录与文件
        for i in range(0, len(dir_list)):
            path = os.path.join(dic_path, dir_list[i])
            if os.path.isdir(path):
                _files.extend(self.list_all_files(path))
            if os.path.isfile(path):
                _files.append(path)
        return _files

    @staticmethod
    def one_min_bar(code, df):
        vol = 0
        bar_list = []
        day_str = df['day'].iloc[0]
        date_list = [format(day_str + ' 09:30:00'), format(day_str + ' 13:00:00'), format(day_str + ' 09:31:00'),
                     format(day_str + ' 13:01:00')]
        am_open_bar = None
        for index, row in df.iterrows():
            _bar = Bar(row, code)
            str_bar_date = row['date'].strftime("%Y-%m-%d %H:%M:%S")
            if str_bar_date == date_list[0]:
                am_open_bar = _bar
                continue
            if str_bar_date == date_list[1]:
                continue
            if str_bar_date == date_list[2]:
                _bar.add_bar(am_open_bar, 2)
            _bar.date = _bar.date + datetime.timedelta(minutes=-1)
            bar_list.append(_bar)
            vol = vol + _bar.volume
        if vol > 0:
            return bar_list
        else:
            return None

    @staticmethod
    def compose_bar(bar_list, cycle):
        new_bar_list = []
        temp_bar = None
        for i in bar_list:
            if temp_bar is None:
                temp_bar = copy.copy(i)
                continue
            else:
                time = i.date - temp_bar.date
                if time.seconds < cycle:
                    temp_bar.add_bar(i)
                else:
                    new_bar_list.append(temp_bar)
                    temp_bar = copy.copy(i)
        if temp_bar not in new_bar_list:
            new_bar_list.append(copy.copy(temp_bar))
        return new_bar_list

if __name__ == '__main__':
    q = Queue()
    while True:
        var = q.get()
        print(var)