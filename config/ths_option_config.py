WARN_USERS = ["XZR"]

DAY_PARAM = 'open;high;low;close;volume;amount;openInterest;positionChange'
DAY_ORDER = ['thscode', 'time', 'open', 'high', 'low', 'close', 'volume', 'amount', 'openInterest', 'positionChange']

INACTIVE_OPTION_BLOCK_ID = ["171008001001", "171008001002", "171008002001", "171008006001", "171008003001",
                            "171008003002", "171008003003", "171008003004", "171008003007",
                            "171008003005", "171008003006",
                            "171008003008", "171008004001", "171008004002", "171008004004", "171008004003",
                            "171008004005", "171008004006",
                            "171008005001", "171008005002", "171008005003", "171008005004", "171008005005",
                            "171008005006", ]
ACTIVE_OPTION_BLOCK_ID = ["171002001", "171002002", "171003006", "171004001", "171005001", "171005002", "171005003",
                          "171005004", "171005007", "171005005",
                          "171005006", "171005008", "171006001", "171006002", "171006004", "171006003", "171006005",
                          "171006006", "171007001", "171007002", "171007003", "171007004", "171007005", "171007005"]

FILE_DATE_FORMAT = "%Y%m%d"
DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

HISTORY_OPTION_DIR = "../../data/History/Option/"
HISTORY_DAY_PATH = "../../data/history_option_day.csv"
HISTORY_M1_PATH = "../../data/history_option_m1.csv"
HISTORY_START_DATE = "2015-01-01"
HISTORY_END_DATE = "2021-07-31"
# HISTORY_MINUTE_START_DATE = "2017-01-01"
# HISTORY_MINUTE_END_DATE = "2021-07-31"
THS_CONFIG = {
    "user": "stkj020",
    "password": "938222"
}

MYSQL_TRADE_CONFIG = {
    "user": "root",
    "password": "123456",
    "host": "192.168.12.204",
    "port": 3306,
    "db": "quant_trade_info"
}

CLICKHOUSE_CONFIG = {
    "ip": "192.168.12.204:8123",
    "host": "192.168.12.204",
    "port": 9000,  # 8123是http端口 client用9000
    "user": "default",
    "pwd": "",
    "settings": {'use_numpy': True}
}
