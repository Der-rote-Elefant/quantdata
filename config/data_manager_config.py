CLICKHOUSE_CONFIG = {
    "ip":"192.168.12.204:8123",
    "host":"192.168.12.204",
    "port":9000, #8123是http端口 client用9000
    "user":"default",
    "pwd":"",
    "settings":{'use_numpy': True}
}

MYSQL_CONFIG={
    "ip": "192.168.12.204:3306",
    "host":"192.168.12.204",
    "port":3306,
    "user": "root",
    "pwd": "123456",
    "manager_db":"quant_data_info",
    "trade_db":"quant_trade_info"
}