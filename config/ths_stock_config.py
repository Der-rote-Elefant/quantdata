WARN_USERS = ["XZR"]
M1_PARAM = "open;high;low;close;volume;amount"
M1_ORDER = ["thscode", "time", "open", "high", "low", "close", "volume", "amount"]
M1_DICT = {
    "thscode": "code",
    "time": "date",
    "open": "open",
    "high": "high",
    "low": "low",
    "close": "close",
    "volume": "volume",
    "amount": "amount"
}

DAY_DICT = {
    "thscode": "code",
    "time": "date",
    "open": "open",
    "high": "high",
    "low": "low",
    "close": "close",
    "volume": "volume",
    "amount": "amount",
    "turnoverRatio": "turnoverRatio",
    "adj_close": "adj_close",
    "floatCapitalOfAShares": "floatCapitalOfAShares",
    "totalCapital": "totalCapital",
    "pe": "pe",
    "ps": "ps",
    "pcf": "pcf",
    "pb": "pb",
    "avgPrice": "avgPrice",
    "floatCapital": "floatCapital",
    "pe_ttm_index": "pe_ttm_index",
    "pb_mrq": "pb_mrq"
}

M5_COLUMNS = ["code", "date", "open", "high", "low", "close", "volume", "openInterest"]
DAY_COLUMS = ["productid", "date", "open", "high", "low", "close", "volume", "openInterest", "traded_market_value",
              "market_value", "turnover", "adjust_price", "PE_TTM", "PS_TTM", "PC_TTM", "PB", "vwap"]
DAY_PARAM = "open,high,low,close,volume,amount,turnoverRatio,floatCapitalOfAShares,totalCapital,pe,ps,pcf,pb,avgPrice"
INDEX_DAY_PARAM = "open,high,low,close,volume,amount,turnoverRatio,floatCapital,totalCapital,pe_ttm_index,ps,pcf,pb_mrq,avgPrice"
DAY_ORDER = ['thscode', 'time', 'open', 'high', 'low', 'close', 'volume', 'amount', 'turnoverRatio', 'adj_close',
             'floatCapitalOfAShares', 'totalCapital', 'pe', 'ps', 'pcf', 'pb', 'avgPrice']

REAL_PARAM = "open;high;low;latest;volume;amount"
INDEX_DAY_ORDER = ['thscode', 'time', 'open', 'high', 'low', 'close', 'volume', 'amount', 'turnoverRatio', 'adj_close',
                   'floatCapital', 'totalCapital', 'pe_ttm_index', 'ps', 'pcf', 'pb_mrq', 'avgPrice']
ACTIVE_A_BLOCK_ID = "001005010"
INACTIVE_A_BLOCK_ID = "001005334011"
# 股票型ETF,051001006001
# 股票型LOF基金,051001014001
ETF_BLOCK_ID = "051001006001"
LOF_BLOCK_ID = "051001014001"
ST_BLOCK_IDS = ["001005334012", "001005334013"]
FILE_DATE_FORMAT = "%Y%m%d"
DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
LATEST_STOCK_DIR = "../data/Latest/Stock/"
LATEST_INDEX_DIR = "../data/Latest/Index/"
LATEST_ETF_DIR = "../data/Latest/ETF/"
HISTORY_STOCK_DIR = "../../data/History/Stock/"
HISTORY_INDEX_DIR = "../../data/History/Index/"
INDEX_LIST_PATH = "../../static/index.csv"
HISTORY_DAY_PATH = "../../data/history_day.csv"
HISTORY_M1_PATH = "../../data/history_m1.csv"
HISTORY_START_DATE = "2021-08-01"
HISTORY_END_DATE = "2021-08-23"
# HISTORY_MINUTE_START_DATE = "2017-01-01"
# HISTORY_MINUTE_END_DATE = "2021-07-31"
THS_CONFIG = {
    "user": "stkj020",
    "password": "938222"
}

# MONGO_CONFIG = {
#     "user": "root",
#     "password": "Baofang660",
#     "host": "192.168.1.173",
#     "port": 27017,
# }

MYSQL_TRADE_CONFIG = {
    "user": "root",
    "password": "123456",
    "host": "192.168.12.204",
    "port": 3306,
    "db": "quant_trade_info"
}

MYSQL_STOCK_CONFIG = {

}

CLICKHOUSE_CONFIG = {
    "ip":"192.168.12.204:8123",
    "host":"192.168.12.204",
    "port":9000, #8123是http端口 client用9000
    "user":"default",
    "pwd":"",
    "settings":{'use_numpy': True}
}
