import pandas as pd
from clickhouse_driver import Client
from config.data_manager_config import *
from Common.model import *
from dataclasses import dataclass
from typing import List
import os
from Common.common import *
import gc
import datetime
from multiprocessing.pool import ThreadPool
from gevent.pool import Pool
from functools import partial


@dataclass()
class SaveDataContext:
    """
    数据库管理信息:即将存储的数据属于的金融大类，大类分类，分类子类,数据分类，数据项
    """
    financial_category: str
    financial_category_describe: str
    financial_class: str
    financial_class_describe: str
    financial_class_member: List[str]
    financial_class_member_describe: List[str]
    data_class: str
    data_class_describe: str
    data_class_member: List[str]
    data_class_member_describe: List[str]
    period: DataPeriod


class DataContextResolver(object):
    def __init__(self, mysql_engine, ck_engine):
        self.engine = mysql_engine
        self.session = sessionmaker(self.engine)

        self.ck_engine = ck_engine

    def resolve_data_context(self, context):
        mysql_manager_client = self._edit_mysql_db(context)
        clickhouse_manager_client, clickhouse_store_client = self._edit_clickhouse_db(context)
        return mysql_manager_client, clickhouse_manager_client, clickhouse_store_client

    def _edit_mysql_db(self, context):
        session = self.session()
        # 金融大类
        add = False
        financial_category = session.query(FinancialCategory).filter(
            FinancialCategory.category == context.financial_category).first()
        if not financial_category:
            add = True
            financial_category = FinancialCategory(category=context.financial_category,
                                                   describe=context.financial_category_describe)
        # 大类分类
        financial_class = session.query(FinancialClass).filter(
            FinancialClass.cls == context.financial_class).first()
        if not financial_class:
            financial_class = FinancialClass(cls=context.financial_class, describe=context.financial_class_describe)

        # 分类子类
        # flt = [FinancialClassMember.classmember == member for member in context.financial_class_member]
        # financial_class_members = session.query(FinancialClassMember).filter(or_(*flt))
        financial_class_members = []
        for member, describe in zip(context.financial_class_member, context.financial_class_member_describe):
            financial_class_member = session.query(FinancialClassMember).filter(
                FinancialClassMember.classmember == member).first()
            if not financial_class_member:
                financial_class_member = FinancialClassMember(classmember=member, describe=describe)
            financial_class_members.append(financial_class_member)

        # 数据分类
        data_class = session.query(DataClass).filter(
            DataClass.cls == context.data_class).first()
        if not data_class:
            data_class = DataClass(cls=context.data_class, describe=context.data_class_describe)

        # 数据项
        data_class_members = []
        for member, describe in zip(context.data_class_member, context.data_class_member_describe):
            data_class_member = session.query(DataClassMember).filter(DataClassMember.classmember == member).first()
            if not data_class_member:
                data_class_member = DataClassMember(classmember=member, describe=describe)
            data_class_members.append(data_class_member)

        # 修改数据库表
        for data_class_member in data_class_members:
            if data_class_member not in data_class.classmembers:
                data_class.classmembers.append(data_class_member)
        for financial_class_member in financial_class_members:
            if financial_class_member not in financial_class.classmembers:
                financial_class.classmembers.append(financial_class_member)

        if data_class not in financial_category.data_classes:
            financial_category.data_classes.append(data_class)

        if financial_class not in financial_category.financial_classes:
            financial_category.financial_classes.append(financial_class)

        if add:
            session.add(financial_category)
        session.commit()
        session.close()
        return None

    def _edit_clickhouse_db(self, context):
        period = context.period.value
        financial_category = context.financial_category
        financial_class = context.financial_class
        financial_class_member = context.financial_class_member
        data_class = context.data_class

        # 数据存储库
        store_database_name = f"{financial_category}_data_{data_class}_{period}"
        # 数据管理库
        manager_database_name = f"{financial_category}_manager_{period}"
        try:
            store_sql = f"CREATE DATABASE IF NOT EXISTS {store_database_name}"
            manager_sql = f"CREATE DATABASE IF NOT EXISTS {manager_database_name}"
            print(store_sql)
            self.ck_engine.execute(store_sql)
            print(manager_sql)
            self.ck_engine.execute(manager_sql)

        except Exception as e:
            print("create database err!!!--->", e.__traceback__)
            return None
        # clickhouse_manager_client,clickhouse_store_client
        clickhouse_manager_client = Client(host=self.ck_engine.connection.host, port=self.ck_engine.connection.port,
                                           user=self.ck_engine.connection.user,
                                           database=manager_database_name, password=self.ck_engine.connection.password,
                                           settings={'use_numpy': True})
        dict_clickhouse_manager_client = {"host": self.ck_engine.connection.host,
                                          "port": self.ck_engine.connection.port,
                                          "user": self.ck_engine.connection.user,
                                          "database": manager_database_name,
                                          "password": self.ck_engine.connection.password,
                                          "settings": {'use_numpy': True}}
        clickhouse_store_client = Client(host=self.ck_engine.connection.host, port=self.ck_engine.connection.port,
                                         user=self.ck_engine.connection.user,
                                         database=store_database_name, password=self.ck_engine.connection.password,
                                         settings={'use_numpy': True})
        dict_clickhouse_store_client = {"host": self.ck_engine.connection.host, "port": self.ck_engine.connection.port,
                                        "user": self.ck_engine.connection.user,
                                        "database": store_database_name, "password": self.ck_engine.connection.password,
                                        "settings": {'use_numpy': True}}

        for member in financial_class_member:
            manager_table_sql = f"CREATE TABLE IF NOT EXISTS {manager_database_name}.{member} " \
                                f"(date DateTime64,code String) " \
                                f"ENGINE = ReplacingMergeTree() ORDER BY (date,code);"
            # ORDER BY (date,code) PRIMARY key id --> id + code 分组去重

            print(manager_table_sql)
            clickhouse_manager_client.execute(manager_table_sql)

        return dict_clickhouse_manager_client, dict_clickhouse_store_client


class DataSaveHelp(object):
    def __init__(self, data_path, b_month_type_data, mysql_engine=None, ck_engine=None, ):
        """
        :param data_path:
        :param mysql_engine:
        :param ck_engine:
        :param b_month_type_data: 存储的数据是否以月份文件存在 False按照code存储处理
        """
        self.month_type = b_month_type_data
        self.data_path = data_path
        self.data_context_resolver = DataContextResolver(mysql_engine, ck_engine)

    def _save2ck(self, data, financial_class_member, clickhouse_manager_client, clickhouse_store_client):
        code = data["code"][0]
        manager_df = data[["date", "code"]]
        store_df = data
        # 在manager表中存储code
        for member in financial_class_member:
            clickhouse_manager_client.insert_dataframe(f"INSERT INTO `{member}` VALUES", manager_df)

        create_store_table = f"CREATE TABLE IF NOT EXISTS `{code}` (date DateTime64,code String) ENGINE = ReplacingMergeTree() ORDER BY (date,code);"
        clickhouse_store_client.execute(create_store_table)
        columns = data.columns
        for column in columns:
            add_columns = f"ALTER TABLE `{code}` ADD COLUMN IF NOT EXISTS {column} Float32"
            clickhouse_store_client.execute(add_columns)
        sql = f"select distinct name from system.columns where database = '{clickhouse_store_client.connection.database}' and table = '{code}'"
        result = clickhouse_store_client.execute(sql)
        db_columns = [i[0] for i in result]
        store_df = store_df[db_columns]
        clickhouse_store_client.insert_dataframe(f"INSERT INTO `{code}` VALUES", store_df)
        pass

    def _save_file(self, context, mysql_manager_client, clickhouse_manager_client, clickhouse_store_client):
        """
        数据按月分文件夹
        :return:
        """
        financial_class_member = context.financial_class_member
        month_dirs = []
        for root, dirs, files in os.walk(self.data_path):
            month_dirs = dirs
            break
        for month_dir in month_dirs:
            temp_path = os.path.join(self.data_path, month_dir)
            print(temp_path)
            t1 = datetime.datetime.now()
            for root, dirs, files in os.walk(temp_path):
                for file in files:
                    df = pd.read_csv(os.path.join(self.data_path, month_dir, file)).astype(str)
                    df["code"] = os.path.splitext(file)[0]
                    df.rename(columns={"time": "date"}, inplace=True)
                    self._save2ck(df, financial_class_member, clickhouse_manager_client, clickhouse_store_client)
                    del df
            t2 = datetime.datetime.now()
            gc.collect()
            print("耗时:", t2 - t1)

    # 异步存储
    def _save_file_coroutine(self, file, month_dir, financial_class_member, clickhouse_manager_client,
                             clickhouse_store_client):
        df = pd.read_csv(os.path.join(self.data_path, month_dir, file)).astype(str)
        df["code"] = os.path.splitext(file)[0]
        df.rename(columns={"time": "date"}, inplace=True)
        self._save2ck(df, financial_class_member, clickhouse_manager_client, clickhouse_store_client)
        del df

    # def set_process_pool(self, process_pool):
    #     self.process_pool = process_pool

    def _save_file_process(self, month_dir, context, mysql_manager_client, clickhouse_manager_client,
                           clickhouse_store_client):

        financial_class_member = context.financial_class_member

        clickhouse_manager_client = Client(host=clickhouse_manager_client["host"],
                                           port=clickhouse_manager_client["port"],
                                           user=clickhouse_manager_client["user"],
                                           database=clickhouse_manager_client["database"],
                                           password=clickhouse_manager_client["password"],
                                           settings=clickhouse_manager_client["settings"])
        clickhouse_store_client = Client(host=clickhouse_store_client["host"], port=clickhouse_store_client["port"],
                                         user=clickhouse_store_client["user"],
                                         database=clickhouse_store_client["database"],
                                         password=clickhouse_store_client["password"],
                                         settings=clickhouse_store_client["settings"])

        temp_path = os.path.join(self.data_path, month_dir)
        print(temp_path)
        t1 = datetime.datetime.now()
        # 每个文件放在一个协程处理
        coroutine_pool = Pool(4000)
        partial_fun = partial(self._save_file_coroutine, month_dir=month_dir,
                              financial_class_member=financial_class_member,
                              clickhouse_manager_client=clickhouse_manager_client,
                              clickhouse_store_client=clickhouse_store_client)

        for root, dirs, files in os.walk(temp_path):
            coroutine_pool.map(partial_fun, files)
        t2 = datetime.datetime.now()
        gc.collect()
        print("耗时:", t2 - t1)

    def _faster_save_file(self, context, mysql_manager_client, clickhouse_manager_client, clickhouse_store_client):
        """
        每个月数据放在一个进程  每个月中的文件作为协程读写 （多进程需要将数据序列化以在进程间传递 用线程替代吧）
        """
        process_pool = ThreadPool(12)

        if self.month_type:
            month_dirs = []
            for root, dirs, files in os.walk(self.data_path):
                month_dirs = dirs
                print(month_dirs)
                break

            # 数据库连接池  每个线程分配一个链接 或者每次线程中新建
            process_pool.map(partial(self._save_file_process, context=context, mysql_manager_client=mysql_manager_client,
                                     clickhouse_manager_client=clickhouse_manager_client,
                                     clickhouse_store_client=clickhouse_store_client), month_dirs)

            process_pool.close()
            process_pool.join()
        else:
            process_pool.map(partial(self._save_file_process, context=context, mysql_manager_client=mysql_manager_client,
                        clickhouse_manager_client=clickhouse_manager_client,
                        clickhouse_store_client=clickhouse_store_client), [self.data_path,])

            process_pool.close()
            process_pool.join()

    def start(self, context):
        mysql_manager_client, clickhouse_manager_client, clickhouse_store_client = self.data_context_resolver.resolve_data_context(
            context)
        # self._save_file(context,mysql_manager_client,clickhouse_manager_client,clickhouse_store_client)
        self._faster_save_file(context, mysql_manager_client, clickhouse_manager_client, clickhouse_store_client)


if __name__ == '__main__':
    MYSQL_DB_URI = f"mysql+pymysql://{MYSQL_CONFIG['user']}:{MYSQL_CONFIG['pwd']}@{MYSQL_CONFIG['ip']}/{MYSQL_CONFIG['manager_db']}?charset=utf8"
    mysql_engine = create_engine(MYSQL_DB_URI)
    clickhouse_engine = Client(host=CLICKHOUSE_CONFIG['host'], port=CLICKHOUSE_CONFIG['port'],
                               user=CLICKHOUSE_CONFIG['user'], database="system", password=CLICKHOUSE_CONFIG['pwd'])


    # mysql量化数据管理 建立相应映射

    # # 所有股票(股票->股票指数->所有股票)
    # data_path = r"D:\code\ShenTuData\data\History\Stock\Day"
    # stock_save_context = SaveDataContext(financial_category="stock",
    #                                      financial_category_describe="股票",
    #                                      data_class="quotation",
    #                                      data_class_describe="行情",
    #                                      data_class_member=["open", "high", "low", "close", "volume", "amount",
    #                                                         "turnoverRatio", "adj_close", "floatCapitalOfAShares",
    #                                                         "totalCapital", "pe", "ps", "pcf", "pb", "avgPrice"],
    #                                      data_class_member_describe=["开盘价", "收盘价", "最低价", "最高价", "成交量", "成交额",
    #                                                                  "换手率", "后复权收盘价", "floatCapitalOfAShares",
    #                                                                  "totalCapital", "市盈率", "市销率", "市现率", "市净率", "均价"],
    #                                      financial_class="stock_index",
    #                                      financial_class_describe="股票指数",
    #                                      financial_class_member=["all_stock"],
    #                                      financial_class_member_describe=["所有股票"],
    #                                      period=DataPeriod.Day)

    # 所有股票指数(指数->股票指数->所有股票指数)
    # data_path = r"D:\code\ShenTuData\data\History\Index\Day"
    # stock_index_save_context = SaveDataContext(financial_category="index",
    #                                            financial_category_describe="指数",
    #                                            data_class="quotation",
    #                                            data_class_describe="行情",
    #                                            data_class_member=["open", "high", "low", "close", "volume", "amount",
    #                                                               "turnoverRatio", "adj_close", "floatCapital",
    #                                                               "totalCapital", "pe_ttm_index", "ps", "pcf", "pb_mrq",
    #                                                               "avgPrice"],
    #                                            data_class_member_describe=["开盘价", "收盘价", "最低价", "最高价", "成交量", "成交额",
    #                                                                        "换手率", "后复权收盘价", "流通市值",
    #                                                                        "totalCapital", "pe_ttm_index", "市销率", "市现率",
    #                                                                        "pb_mrq", "均价"],
    #                                            financial_class="index_of_stock",
    #                                            financial_class_describe="股票指数",
    #                                            financial_class_member=["all_stock_index"],
    #                                            financial_class_member_describe=["所有股票指数"],
    #                                            period=DataPeriod.Day)



    data_path = r"D:\code\ShenTuData\data\History\Option\Day"
    option_save_context = SaveDataContext(financial_category="option",
                                               financial_category_describe="期权",
                                               data_class="quotation",
                                               data_class_describe="行情",
                                               data_class_member=["open", "high", "low", "close", "volume", "amount",
                                                                  "openInterest", "positionChange"],
                                               data_class_member_describe=["开盘价", "收盘价", "最低价", "最高价", "成交量", "成交额",
                                                                           "持仓量", "持仓变动"],
                                               financial_class="option_index",
                                               financial_class_describe="期权指数",
                                               financial_class_member=["all_option"],
                                               financial_class_member_describe=["所有期权"],
                                               period=DataPeriod.Day)


    data_help = DataSaveHelp(data_path,False, mysql_engine, clickhouse_engine)
    data_help.start(option_save_context)
