from iFinDPy import *
from datetime import datetime
from datetime import date
import pandas as pd
import os
import math
from dateutil.relativedelta import relativedelta
import sys
from util.wechat import Wechat
from config.ths_option_config import *

from DataApi.ths_api import ThsApi

message = Wechat(users=WARN_USERS)


def get_m1_bar(stock_code, start_date_str, end_date_str, param, column_order):
    single_year = 2
    start_time = datetime.strptime(start_date_str, DATETIME_FORMAT)
    end_time = datetime.strptime(end_date_str, DATETIME_FORMAT)
    query_count = math.ceil((end_time.year - start_time.year + 1) / single_year)
    df_m1 = pd.DataFrame()
    for i in range(query_count):
        temp_start_time = start_time + relativedelta(years=+(i * single_year))
        start_time_str = temp_start_time.strftime(DATETIME_FORMAT)
        if i == query_count - 1:
            end_time_str = end_time.strftime(DATETIME_FORMAT)
        else:
            temp_end_time = temp_start_time + relativedelta(years=+single_year) + relativedelta(days=-1)
            temp_end_time = datetime(temp_end_time.year, temp_end_time.month, temp_end_time.day, 15, 15, 0)
            end_time_str = temp_end_time.strftime(DATETIME_FORMAT)

        print(stock_code, param, start_time_str, end_time_str)
        ths_m1_sequence = THS_HighFrequenceSequence(stock_code, param,
                                                    'CPS:0,MaxPoints:50000,Fill:Previous,Interval:1',
                                                    start_time_str, end_time_str)
        if ths_m1_sequence["errorcode"] == 0:
            ths_m1 = THS_Trans2DataFrame(ths_m1_sequence)
            if not ths_m1.empty:
                ths_m1["date_time"] = ths_m1["time"]  # .apply(lambda x: x)  # 作为行索引方便以日期过滤
                ths_m1['date_time'] = pd.to_datetime(ths_m1['date_time'])
                ths_m1 = ths_m1.set_index("date_time")  # 将时间作为索引列
                df_m1 = df_m1.append(ths_m1)
        elif ths_m1_sequence["errorcode"] == -4302:
            global current_user
            print(current_user + "数据超出限制")
            message.send_message(current_user + "数据超出限制")
            users[current_user]["is_limit"] = True
            THS_iFinDLogout()
            result = login()
            if result:
                ths_m1_sequence = THS_HighFrequenceSequence(stock_code, param,
                                                            'CPS:0,MaxPoints:50000,Fill:Previous,Interval:1',
                                                            start_time_str, end_time_str)
                if ths_m1_sequence["errorcode"] == 0:
                    ths_m1 = THS_Trans2DataFrame(ths_m1_sequence)
                    if not ths_m1.empty:
                        ths_m1["date_time"] = ths_m1["time"]  # .apply(lambda x: x)  # 作为行索引方便以日期过滤
                        ths_m1['date_time'] = pd.to_datetime(ths_m1['date_time'])
                        ths_m1 = ths_m1.set_index("date_time")  # 将时间作为索引列
                        df_m1 = df_m1.append(ths_m1)
            else:
                message.send_message("数据超出限制")
                sys.exit(0)

    if not df_m1.empty:
        df_m1.sort_index()
        df_m1 = df_m1[column_order]
    return df_m1



class ThsDownloadHelp(object):

    def __init__(self,api):
        self.api = api

    def get_option_list(self):
        option_list = list()
        date_str = datetime.today().strftime(DATE_FORMAT)
        blocks = INACTIVE_OPTION_BLOCK_ID + ACTIVE_OPTION_BLOCK_ID
        for block_id in blocks:
            option_pool = self.api.get_block_stocks(block_id, date_str)
            option_list.extend(option_pool["THSCODE"])
        return option_list



    def download_day_fun(self,option_list):
        param = DAY_PARAM
        column_order = list(DAY_ORDER)
        option_path = "%s%s" % (HISTORY_OPTION_DIR, "Day")

        column_order.remove("thscode")
        current = 0

        if not os.path.exists(HISTORY_DAY_PATH):
            df_record = pd.DataFrame()
            df_record.to_csv(HISTORY_DAY_PATH)

        for option_code in option_list:
            df_record = pd.read_csv(HISTORY_DAY_PATH, header=None)
            if not df_record.empty:
                if option_code not in df_record[0].values:
                    try:
                        option_df = self.api.get_day_bar([option_code], HISTORY_START_DATE, HISTORY_END_DATE, param, column_order)
                        self.save_to_csv(option_code, option_df, option_path, HISTORY_DAY_PATH)
                    except Exception as e:
                        print(option_code)
                        print(e)

            current += 1
            print("%d/%d" % (current, len(option_list)))

    def download_minute_fun(self,option_list):
        pass

    def save_to_csv(self,stock_code, df, file_path, history_path, month_save=False):
        if month_save:
            # 按月输出bar结果
            start_date = datetime.strptime(HISTORY_START_DATE, DATE_FORMAT)
            end_date = datetime.strptime(HISTORY_END_DATE, DATE_FORMAT)
            temp_start = start_date
            while temp_start <= end_date:
                temp_end = temp_start + relativedelta(months=+1) + relativedelta(days=-1)
                start = temp_start.strftime(DATE_FORMAT)
                end = temp_end.strftime(DATE_FORMAT)
                curr_df = df[start:end]
                if not curr_df.empty:
                    dir_name = "%s%s/" % (str(temp_start.year), str(temp_start.month).zfill(2))
                    dir_name = os.path.join(file_path, dir_name)
                    file_name = "%s.csv" % stock_code
                    if not os.path.exists(dir_name):
                        os.makedirs(dir_name)
                    #curr_df.to_csv(os.path.join(dir_name, file_name), header=None, index=0)
                    curr_df.to_csv(os.path.join(dir_name, file_name))
                temp_start = temp_start + relativedelta(months=+1)
        else:
            # 按code存储
            if not os.path.exists(file_path):
                os.makedirs(file_path)
            file_name = "%s.csv" % stock_code
            df.to_csv(os.path.join(file_path, file_name))


        if not df.empty:
            df_record = pd.read_csv(history_path, header=None)
            df_record = df_record.append([stock_code, ])
            df_record.to_csv(history_path, header=None, index=0)

    def download_history(self,period):
        try:
            print('download_history-startTime:%s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            result = self.api.login()
            if result:
                # 下载需要的数据
                option_list = self.get_option_list()

                if period == "M1":
                    self.download_minute_fun(option_list)
                elif period == "Day":
                    self.download_day_fun(option_list)

                self.api.logout()
            else:
                print("登录失败")
                error_msg = "同花顺下载历史登录失败"
                message.send_message(error_msg)
            print('download_history:%s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        except Exception as e:
            error_msg = "同花顺下载历史数据出错：%s" % e
            message.send_message(error_msg)
            raise e





if __name__ == '__main__':
    ths_api = ThsApi(THS_CONFIG['user'], THS_CONFIG['password'])
    ths_download_history = ThsDownloadHelp(ths_api)
    ths_download_history.download_history("Day")