from iFinDPy import *
from datetime import datetime
from datetime import date
import pandas as pd
import os
import math
from dateutil.relativedelta import relativedelta
import sys
from util.wechat import Wechat
from config.ths_stock_config import *

from DataApi.ths_api import ThsApi

message = Wechat(users=WARN_USERS)


class ThsDownloadHelp(object):

    def __init__(self,api):
        self.api = api


    def get_stock_list(self):
        stock_list = list()
        date_str = datetime.today().strftime(DATE_FORMAT)
        for block_id in [ACTIVE_A_BLOCK_ID,INACTIVE_A_BLOCK_ID]:
            stock_pool = self.api.get_block_stocks(block_id, date_str)
            stock_list.extend(stock_pool["THSCODE"])
        #stock_list = ["000001.SZ"]
        return stock_list

    def get_index_list(self):
        index_list = list(pd.read_csv(INDEX_LIST_PATH, header=None)[0])
        #index_list = []
        return index_list


    def download_all_day_bar(self,stock_list, index_list):
        self.download_day_fun(index_list, True)
        self.download_day_fun(stock_list,False)

    def download_all_m1_bar(self,stock_list, index_list):
        self.download_minute_fun(index_list, is_index=True)
        self.download_minute_fun(stock_list, is_index=False)


    def download_day_fun(self,stock_list,is_index):
        if is_index:
            param = INDEX_DAY_PARAM
            column_order = list(INDEX_DAY_ORDER)
            stock_path = "%s%s" % (HISTORY_INDEX_DIR, "Day")
        else:
            param = DAY_PARAM
            column_order = list(DAY_ORDER)
            stock_path = "%s%s" % (HISTORY_STOCK_DIR, "Day")

        column_order.remove("thscode")
        current = 0

        if not os.path.exists(HISTORY_DAY_PATH):
            df_record = pd.DataFrame()
            df_record.to_csv(HISTORY_DAY_PATH)

        for stock_code in stock_list:
            df_record = pd.read_csv(HISTORY_DAY_PATH, header=None)
            if not df_record.empty:
                if stock_code not in df_record[0].values:
                    try:
                        stock_df = self.api.get_day_bar([stock_code], HISTORY_START_DATE, HISTORY_END_DATE, param, column_order, is_index=is_index)
                        self.save_to_csv(stock_code, stock_df, stock_path, HISTORY_DAY_PATH, is_index=is_index)
                    except Exception as e:
                        print(stock_code)
                        print(e)

            current += 1
            print("%d/%d" % (current, len(stock_list)))

    def download_minute_fun(self,stock_list,is_index):
        if is_index:
            stock_path = "%s%s" % (HISTORY_INDEX_DIR, "M1")
        else:
            stock_path = "%s%s" % (HISTORY_STOCK_DIR, "M1")

        start_time = datetime.strptime(HISTORY_START_DATE, DATE_FORMAT)
        end_time = datetime.strptime(HISTORY_END_DATE, DATE_FORMAT)
        start_str = datetime(start_time.year, start_time.month, start_time.day, 9, 15, 0).strftime(DATETIME_FORMAT)
        end_str = datetime(end_time.year, end_time.month, end_time.day, 15, 15, 0).strftime(DATETIME_FORMAT)
        m1_order = list(M1_ORDER)
        m1_order.remove("thscode")
        current = 0

        if not os.path.exists(HISTORY_M1_PATH):
            df_record = pd.DataFrame()
            df_record.to_csv(HISTORY_M1_PATH)

        for stock_code in stock_list:
            df_record = pd.read_csv(HISTORY_M1_PATH, header=None)
            if df_record.empty or stock_code not in df_record[0].values:
                try:
                    stock_df = self.api.get_minute_bar( [stock_code], start_str, end_str, M1_PARAM, m1_order)
                    self.save_to_csv(stock_code, stock_df, stock_path, HISTORY_M1_PATH, is_index=is_index)
                except Exception as e:
                    print(stock_code)
                    print(e)

            current += 1
            print("%d/%d" % (current, len(stock_list)))

    def save_to_csv(self,stock_code, df, file_path, history_path, is_index=False):
        # 按月输出bar结果
        start_date = datetime.strptime(HISTORY_START_DATE, DATE_FORMAT)
        end_date = datetime.strptime(HISTORY_END_DATE, DATE_FORMAT)
        temp_start = start_date
        while temp_start <= end_date:
            temp_end = temp_start + relativedelta(months=+1) + relativedelta(days=-1)
            start = temp_start.strftime(DATE_FORMAT)
            end = temp_end.strftime(DATE_FORMAT)
            curr_df = df[start:end]
            if not curr_df.empty:
                dir_name = "%s%s/" % (str(temp_start.year), str(temp_start.month).zfill(2))
                dir_name = os.path.join(file_path, dir_name)
                file_name = "%s.csv" % stock_code
                if not os.path.exists(dir_name):
                    os.makedirs(dir_name)
                #curr_df.to_csv(os.path.join(dir_name, file_name), header=None, index=0)
                curr_df.to_csv(os.path.join(dir_name, file_name))
            temp_start = temp_start + relativedelta(months=+1)

        if not df.empty:
            df_record = pd.read_csv(history_path, header=None)
            df_record = df_record.append([stock_code, ])
            df_record.to_csv(history_path, header=None, index=0)

    def download_history(self,period):
        try:
            print('download_history-startTime:%s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            result = self.api.login()
            if result:
                # 下载需要的数据
                stock_list = self.get_stock_list()
                index_list = self.get_index_list()

                #
                # stock_list = []
                # index_list = []

                if period == "M1":
                    self.download_all_m1_bar(stock_list, index_list)
                elif period == "Day":
                    self.download_all_day_bar(stock_list, index_list)

                self.api.logout()
            else:
                print("登录失败")
                error_msg = "同花顺下载历史登录失败"
                message.send_message(error_msg)
            print('download_history:%s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        except Exception as e:
            error_msg = "同花顺下载历史数据出错：%s" % e
            message.send_message(error_msg)
            raise e

    def start(self):
        pass



if __name__ == '__main__':
    ths_api = ThsApi(THS_CONFIG['user'], THS_CONFIG['password'])
    ths_download_history = ThsDownloadHelp(ths_api)
    ths_download_history.download_history("Day")