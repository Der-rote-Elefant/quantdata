import os
import sys

sys.path.insert(0,os.path.dirname(os.getcwd()))
from PyQt5 import QtCore, QtGui, QtWidgets
from data_manager import DataManager


class MainWindow(QtWidgets.QMainWindow):
    """
    Main window for Data Manager
    """
    def __init__(self, data_manager: DataManager = None):
        super(MainWindow, self).__init__()
        self.data_manager: DataManager = data_manager
        self.window_title: str = "shentu data manager"
        self.init_ui()

    def init_ui(self) -> None:
        self.setWindowTitle(self.window_title)


if __name__ == '__main__':
    window = MainWindow()