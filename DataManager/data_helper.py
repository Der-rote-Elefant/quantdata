from Common.common import *
from Common.model import *
from db_helper import QueryDatabaseHelp
from config.data_manager_config import *
from typing import List
from dataclasses import dataclass
import datetime


@dataclass
class QueryContext:
    period: DataPeriod
    financial_category: str
    data_class: str
    start_time: str
    end_time: str
    codes: List[str] = None
    financial_class_member: str = None
    data_members: List[str] = None


class DataHelper(object):
    def __init__(self, mysql_config, ck_config):
        self.__query_help: QueryDatabaseHelp = QueryDatabaseHelp(mysql_config, ck_config)

    def query_data_from_context(self, query_context):
        # 定位数据库
        query_store_db_name = f"{query_context.financial_category}_data_{query_context.data_class}_{query_context.period.value}"
        query_manager_db_name = f"{query_context.financial_category}_manager_{query_context.period.value}"

        # 查询条件

        start_time = query_context.start_time
        end_time = query_context.end_time

        # codes有数值时直接查询code 无数值查询financial_class_member的成分code
        if query_context.codes:
            codes = query_context.codes
            code_df = None
        else:
            if not query_context.financial_class_member:
                print("查询code和financial_class_member 不能同时为空")
                raise
            # query_manager_db_name查询成员列表
            code_df = self.__query_help.query_codes_of_financial_class_member(query_manager_db_name,
                                                                              query_context.financial_class_member,
                                                                              start_time, end_time)
            codes = set(code_df["code"])

        # data_members有数值时直接查询data_members 无数值查询data_class的所有参数
        if query_context.data_members:
            data_members = query_context.data_members
        else:
            data_members = self.__query_help.query_params_of_data_class_member(query_context.data_class)

        data_df_dict = {}
        t1 = datetime.datetime.now()
        count = 0
        code_count = len(codes)
        for code in codes:
            df = self.__query_help.query_data(query_store_db_name, code, data_members, start_time, end_time)
            data_df_dict[code] = df
            count = count + 1
            print(f"{count}/{code_count}")
        t2 = datetime.datetime.now()
        print(f"查询总共耗时 {t2 - t1}")
        return data_df_dict, code_df

    # 查询金融的大类
    def query_financial_category(self, category: str = "stock") -> FinancialCategory:
        # 金融大类对象
        # 大类分类对象集合
        # 每个大类分类的子类
        # 数据大类集合
        # 每个数据大类的成分
        financial_category = self.__query_help.query_financial_category(category)
        return financial_category

    # 查询数据大类
    def query_data_class(self, cls: str = ""):
        # 数据大类集合
        # 每个大类包含的数据项
        pass


if __name__ == '__main__':
    data_help = DataHelper(MYSQL_CONFIG, CLICKHOUSE_CONFIG)
    # 查询需要的股票行情数据

    # 可选项
    category_name = "stock"
    financial_category = data_help.query_financial_category(category=category_name)
    financial_class_list = financial_category.financial_classes
    data_class_list = financial_category.data_classes

    print(f"{category_name}支持的金融分类:")
    for financial_class in financial_class_list:
        print(f"分类：{financial_class.cls},描述:{financial_class.describe}")
        for member in financial_class.classmembers:
            print(f"--->具体分类子类：{member.classmember},描述:{member.describe}")

    print(f"{category_name}支持的数据分类:")
    for data_class in data_class_list:
        print(f"分类：{data_class.cls},描述:{data_class.describe}")
        for member in data_class.classmembers:
            print(f"--->具体数据项：{member.classmember},描述:{member.describe}")

    # 组装数据适配器
    context = QueryContext(period=DataPeriod.Day, financial_category=category_name,
                           financial_class_member=financial_class_list[0].classmembers[0].classmember,
                           data_class=data_class_list[0].cls, start_time="2010-01-01", end_time="2011-05-01")

    # 查询
    dict_data, df_code = data_help.query_data_from_context(context)

