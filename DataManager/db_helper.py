import pandas as pd

from Common.model import *
from config.data_manager_config import *
from clickhouse_driver import Client
from sqlalchemy.orm import joinedload




class BuildDatabaseHelp(object):
    def __init__(self, mysql_config, ck_config):
        self.mysql_config = mysql_config
        self.ck_config = ck_config

        db_uri = f"mysql+pymysql://{mysql_config['user']}:{mysql_config['pwd']}@{mysql_config['ip']}/{mysql_config['manager_db']}?charset=utf8"
        self.mysql_engine = create_engine(db_uri)

    def init_mysql_orm_db(self):
        Base.metadata.create_all(self.mysql_engine)

    def init_mysql_db(self):
        pass

    def drop_mysql_db(self):
        Base.metadata.drop_all(self.mysql_engine)


class QueryDatabaseHelp(object):
    def __init__(self, mysql_config, ck_config):
        db_uri = f"mysql+pymysql://{mysql_config['user']}:{mysql_config['pwd']}@{mysql_config['ip']}/{mysql_config['manager_db']}?charset=utf8"
        self.mysql_engine = create_engine(db_uri)
        self.mysql_session = sessionmaker(self.mysql_engine)
        self.ck_config = ck_config
        self.dict_ck_client = {}

    def query_financial_category(self, category):
        session = self.mysql_session()
        financial_category = session.query(FinancialCategory).filter(FinancialCategory.category == category).first()
        session.close()
        return financial_category

    def query_financial_class(self):
        session = self.mysql_session()
        q = session.query(FinancialClass).all()
        for financial_class in q:
            print(financial_class.cls, financial_class.describe, financial_class.classmembers[0].classmember)
        session.close()

    def query_codes_of_financial_class_member(self, db_name, table_name, start, end):
        if db_name not in self.dict_ck_client:
            self.dict_ck_client[db_name] = Client(host=self.ck_config['host'], port=self.ck_config['port'],
                                                  user=self.ck_config['user'], database=db_name,
                                                  password=self.ck_config['pwd'])
        sql = f"select * from `{table_name}` where `date` between '{start}' and '{end}' order by date"
        data = self.dict_ck_client[db_name].execute(sql)
        df = pd.DataFrame(columns=['date', 'code'], data=data)
        return df

    def query_params_of_data_class_member(self, data_class_name):
        data_class_members = []
        session = self.mysql_session()
        q = session.query(DataClass).filter(DataClass.cls == data_class_name).first()
        for data_class_member in q.classmembers:
            data_class_members.append(data_class_member.classmember)
        session.close()
        return data_class_members

    def query_data(self, db_name, table_name, columns, start, end):

        if db_name not in self.dict_ck_client:
            self.dict_ck_client[db_name] = Client(host=self.ck_config['host'], port=self.ck_config['port'],
                                                  user=self.ck_config['user'], database=db_name,
                                                  password=self.ck_config['pwd'])
        # all_col = ["date", "code"] + columns
        query_col = f"select distinct name from system.columns where database='{db_name}' and table='{table_name}'"
        col_list, col = self.dict_ck_client[db_name].execute(query_col, columnar=True, with_column_types=True)
        all_col = ["date", "code"]
        for column in columns:
            if column in col_list[0]:
                all_col.append(column)
        sql = f"select {','.join(all_col)} from `{table_name}` where `date` between '{start}' and '{end}'"
        data, df_columns = self.dict_ck_client[db_name].execute(sql, columnar=True, with_column_types=True)
        df = pd.DataFrame({col[0]: d for d, col in zip(data, df_columns)})
        return df


if __name__ == '__main__':
    build_database_help = BuildDatabaseHelp(MYSQL_CONFIG, CLICKHOUSE_CONFIG)
    build_database_help.init_mysql_orm_db()
