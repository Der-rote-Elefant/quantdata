from jqdatasdk import *
import pandas as pd


class JqApi(object):

    def __init__(self, user_id, password):
        self.user_id = user_id
        self.password = password

    def login(self):
        auth(self.user_id, self.password)
        if is_auth():
            return True
        else:
            error_msg = "'聚宽登录失败-error:%d-Time:%s' % (login_result, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))"
            print(error_msg)
            return False

    def logout(self):
        logout()

    def get_a_xr_info(self, day):
        df = finance.run_query(query(finance.STK_XR_XD).filter(finance.STK_XR_XD.a_xr_date == day).order_by(
            finance.STK_XR_XD.a_xr_date))
        if len(df) > 0:
            df = df.loc[:, ["code", "a_xr_date", "dividend_ratio", "transfer_ratio", "bonus_ratio_rmb"]]
            df.rename(columns={
                "code": "Code",
                "a_xr_date": "Date",
                "dividend_ratio": "DividendRatio",
                "transfer_ratio": "TransferRatio",
                "bonus_ratio_rmb": "BonusRatioRmb"
            }, inplace=True)
            df["Code"] = df["Code"].apply(lambda x: x[:6] + ".SK")
            df["Date"] = pd.to_datetime(df["Date"], format="%Y/%m/%d")
            df.fillna(0, inplace=True)
        return df

    def get_etf_xr_info(self, day):
        df = finance.run_query(query(finance.FUND_DIVIDEND).filter(finance.FUND_DIVIDEND.ex_date == day).order_by(
            finance.FUND_DIVIDEND.ex_date))
        if len(df) > 0:
            df = df.loc[:, ["code", "ex_date", "proportion", "split_ratio"]]
            df.rename(columns={
                "code": "Code",
                "ex_date": "Date",
                "proportion": "Proportion",
                "split_ratio": "SplitRatio"
            }, inplace=True)
            df["Code"] = df["Code"].apply(lambda x: x[:6] + ".ETF")
            df["Date"] = pd.to_datetime(df["Date"], format="%Y/%m/%d")
            df.fillna(0, inplace=True)
        return df
