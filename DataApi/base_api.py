from abc import ABC, abstractmethod



class BaseDataApi(ABC):
    def __init__(self,user,password):
        self.user = user
        self.password = password

    @abstractmethod
    def login(self)->None:
        pass

    @abstractmethod
    def logout(self)->None:
        pass

