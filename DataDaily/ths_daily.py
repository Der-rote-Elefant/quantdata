import time
from iFinDPy import *
from datetime import datetime
from datetime import timedelta
import pandas as pd
from config.ths_stock_config import *
from DataApi.ths_api import *
# from DataApi.jq_api import *
import schedule
from util.wechat import DingTalk
import traceback
from util.mysql_db import MysqlDBBase
from clickhouse_driver import *
import time

def check_trade_day(func):
    """
    装饰器 判断是否为交易日
    :param func:
    :return:
    """

    def inner(*args, **kwargs):
        today_str = date.today().strftime(DATE_FORMAT)
        date_str = get_last_trade_day()
        if today_str == date_str:
        # if True:
            ths_api = ThsApi(THS_CONFIG["user"], THS_CONFIG["password"])
            if ths_api.login():
                try:
                    func(ths_api, today_str, *args, **kwargs)
                except Exception as e:
                    message_send = DingTalk()
                    message_send.send_message_to_dingtalk("func:{0}出错,错误信息:{1}".format(func.__name__, traceback.print_exc()))
                ths_api.logout()
            else:
                message_send = DingTalk()
                message_send.send_message_to_dingtalk("func:{0}出错,同花顺登录失败".format(func.__name__))

    return inner


def get_last_trade_day(day=1):
    """
    获取最近一个交易日日期
    :param day:倒数第几个
    :return: 最近一个交易日日期的字符串
    """
    mysql_helper = MysqlDBBase(host=MYSQL_TRADE_CONFIG["host"],
                               port=MYSQL_TRADE_CONFIG["port"],
                               user_id=MYSQL_TRADE_CONFIG["user"],
                               password=MYSQL_TRADE_CONFIG["password"],
                               db_name=MYSQL_TRADE_CONFIG["db"])
    df_date = mysql_helper.query("select Date from trade_date order by date")
    date_str = df_date["Date"].iat[-1 * day].strftime(DATE_FORMAT)
    return date_str


# def exec_jq_api(func):
#     """
#     装饰器 判断是否交易日，登录登出同花顺
#     :param func:
#     :return:
#     """
#
#     def inner(*args, **kwargs):
#         today_str = date.today().strftime(DATE_FORMAT)
#         date_str = get_last_trade_day()
#         if today_str == date_str:
#             jq_api = JqApi("18042485100", "xzr8510776")
#             if jq_api.login():
#                 try:
#                     func(jq_api, today_str, *args, **kwargs)
#                 except Exception as e:
#                     message_send = Wechat(WARN_USERS)
#                     message_send.send_message("func:{0}出错,错误信息:{1}".format(func.__name__, traceback.print_exc()))
#                 jq_api.logout()
#             else:
#                 message_send = Wechat(WARN_USERS)
#                 message_send.send_message("func:{0}出错,聚宽登录失败".format(func.__name__))
#
#     return inner

def update_trade_day():
    """
    更新交易日
    :return:
    """
    ths_api = ThsApi(THS_CONFIG["user"], THS_CONFIG["password"])
    if ths_api.login():
        date_str = date.today().strftime(DATE_FORMAT)
        # yesterday = date.today() - timedelta(days=1)
        # date_str = yesterday.strftime(DATE_FORMAT)

        is_trade_day = ths_api.is_trade_day(date_str)
        if is_trade_day:
            mysql_helper = MysqlDBBase(host=MYSQL_TRADE_CONFIG["host"],
                                       port=MYSQL_TRADE_CONFIG["port"],
                                       user_id=MYSQL_TRADE_CONFIG["user"],
                                       password=MYSQL_TRADE_CONFIG["password"],
                                       db_name=MYSQL_TRADE_CONFIG["db"])
            df = pd.DataFrame({"Date": [date.today(), ]})
            # df = pd.DataFrame({"Date": [yesterday, ]})
            mysql_helper.save(table_name="trade_date", df=df)

            pass

        ths_api.logout()
        print(datetime.now(), "更新交易日完成")
        message_send = DingTalk()
        message_send.send_message_to_dingtalk("每日定时任务-->更新交易日完成")

    else:
        message_send = DingTalk()
        message_send.send_message_to_dingtalk("func出错,同花顺登录失败")


@check_trade_day
def update_day_data(ths_api, today_str, is_yesterday=False):
    """
    更新日线数据
    :return:
    """
    message_send = DingTalk()
    # message_send.send_message_to_dingtalk("每日定时任务-->开始存储股票日线数据")
    try:
        if is_yesterday:
            today_str = get_last_trade_day(2)
        today = datetime.strptime(today_str, DATE_FORMAT)
        stock_list = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, today_str, is_replace=False)
        stock_list = list(stock_list["THSCODE"])
        index_list = list(pd.read_csv(r"../static/index.csv", header=None)[0])
        df_index = ths_api.get_day_bar(index_list, today_str, today_str, INDEX_DAY_PARAM, INDEX_DAY_ORDER,
                                       is_index=True, set_time_index=False)
        df_stock = ths_api.get_day_bar(stock_list, today_str, today_str, DAY_PARAM, DAY_ORDER, is_index=False,
                                       set_time_index=False)

        stock_file_name = "%s.csv" % today.strftime(FILE_DATE_FORMAT)
        if not os.path.exists(os.path.join(LATEST_INDEX_DIR, "Day")):
            os.makedirs(os.path.join(LATEST_INDEX_DIR, "Day"))
        if not os.path.exists(os.path.join(LATEST_STOCK_DIR, "Day")):
            os.makedirs(os.path.join(LATEST_STOCK_DIR, "Day"))
        df_index.to_csv(os.path.join(LATEST_INDEX_DIR, "Day", stock_file_name),
                        index=0)
        df_stock.to_csv(os.path.join(LATEST_STOCK_DIR, "Day", stock_file_name),
                        index=0)
        df_stock.rename(columns=DAY_DICT, inplace=True)
        df_index.rename(columns=DAY_DICT, inplace=True)

        # df_stock = df_stock[DAY_COLUMS]
        # df_index = df_index[DAY_COLUMS]

        # 存储
        db_name = "stock_data_quotation_day"
        df_stock = df_stock.fillna(0)
        df_index = df_index.fillna(0)
        save(df_index, is_index=True)
        save(df_stock)
        print("当日日线存储完成")
        os.remove(os.path.join(LATEST_INDEX_DIR, "Day", stock_file_name))
        os.remove(os.path.join(LATEST_STOCK_DIR, "Day", stock_file_name))
        message_send.send_message_to_dingtalk("每日定时任务-->存储股票日线数据成功")
    except Exception as e:
        message_send.send_message_to_dingtalk("func:{0}出错,错误信息:{1}".format("update_day_data", traceback.print_exc()))




def save(df, is_index=False):
    #df = df.astype({'Max Humidity':'float64','Max Dew PointF':'float64'})
    # 成分表
    df_code = df[["date", "code"]]
    test_db = "cktest"
    test_table = "all"
    if is_index:
        manager_db = "index_manager_day"
        manager_table = "all_stock_index"
        data_db = "index_data_quotation_day"
    else:
        manager_db = "stock_manager_day"
        manager_table = "all_stock"
        data_db = "stock_data_quotation_day"

    manager_client = Client(host=CLICKHOUSE_CONFIG['host'], port=CLICKHOUSE_CONFIG['port'],
                            user=CLICKHOUSE_CONFIG['user'], database=manager_db,
                            password=CLICKHOUSE_CONFIG['pwd'], settings=CLICKHOUSE_CONFIG['settings'])
    manager_client.insert_dataframe(f"INSERT INTO `{manager_table}` VALUES", df_code)
    # 数据表

    data_client = Client(host=CLICKHOUSE_CONFIG['host'], port=CLICKHOUSE_CONFIG['port'],
                         user=CLICKHOUSE_CONFIG['user'], database=data_db,
                         password=CLICKHOUSE_CONFIG['pwd'])
    columns = df.columns
    for index, row in df.iterrows():
        # 判断code表是否存在 结构参考000001.SZ表  判断是否以及存储过当日数据 最后进行入库
        code = row["code"]
        print(code)
        #
        create_store_table = f"CREATE TABLE IF NOT EXISTS `{code}` (date DateTime64,code String) ENGINE = ReplacingMergeTree() ORDER BY (date,code);"
        data_client.execute(create_store_table)

        sql = f"select distinct name from system.columns where database = '{data_client.connection.database}' and table = '{code}'"
        result = data_client.execute(sql)
        if not set(result) > set(columns):
            for column in columns:
                add_columns = f"ALTER TABLE `{code}` ADD COLUMN IF NOT EXISTS {column} Float32"
                data_client.execute(add_columns)

        row_dict = row.to_dict()
        row_dict["date"] = datetime.strptime(row_dict["date"], DATE_FORMAT)
        if "floatCapital" in row_dict:
            row_dict["floatCapital"] = float(row_dict["floatCapital"])
        if "totalCapital" in row_dict:
            row_dict["totalCapital"] = float(row_dict["totalCapital"])
        data_client.execute(f"INSERT INTO `{code}`({','.join(columns)}) VALUES", [row_dict, ])


def update_minute_data():
    """
    更新分钟数据
    :return:
    """


def update_block():
    """
    更新板块
    :return:
    """
    # update_index_component()
    # update_stock_list()
    # update_st()
    # update_credit_list()
    # update_exit_list()
    pass


def main():
    """
    同花顺接口 每日定时任务
    :return:
    """
    print("开启定时任务")
    schedule.every().day.at("15:15").do(update_trade_day)
    schedule.every().day.at("16:08").do(update_day_data)

    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == '__main__':
    # df_data = pd.read_csv(r"D:\code\ShenTuData\data\History\Stock\Day\201801\002889.SZ.csv")
    # df_data["code"] = "000002.SZ"
    # df_data.rename(columns=DAY_DICT, inplace=True)
    # save(df_data)

    main()
    # update_trade_day()
    # update_day_data()
